# Panel programme
#
# Created: March 2015
#      by: Manolo Broseta at ALBA Computing Division
#

import time, sys, select, math
from paneldrv import *
from image_res import *

class Panel_Off():
	def __init__(self, parent=None):
		self.drv = PanelDriver()
		
		self.drv.ClearScreen()
		self.drv.SendData('#TC 0')
		self.drv.PanelSettings(bright=70)		
		
		self.drv.DrawImage(40,0,Face_image)
		
		self.drv.SendData('#ZB 1')
		self.drv.SendData('#ZL 0,20,BYE\n')
		self.drv.SendData('#ZL 10,30,BYE\n')
		self.drv.SendData('#ZL 90,20,BYE\n')
		self.drv.SendData('#ZL 100,30,BYE\n')
		
		time.sleep(5)
		self.drv.SendData('#PD 0,0')		
		self.drv.ClearScreen()

if __name__ == "__main__":
	
	
	import sys, select
	
	args = ' '.join(sys.argv[1:])
	Work = Panel_Off()
	
	sys.exit()

	
		
	
